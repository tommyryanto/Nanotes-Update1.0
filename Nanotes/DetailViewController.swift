//
//  DetailViewController.swift
//  Nanotes
//
//  Created by Tommy Ryanto on 9/3/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit
import Social

class DetailViewController: UIViewController {
    
    var titlee: String?
    var content: String?
    var note: Notes?
    
    @IBAction func backToDetail(sender: UIStoryboardSegue){
        
    }
    
    @IBAction func shareBtn(_ sender: Any) {
//        let share = UIAlertController(title: "Share", message: "", preferredStyle: .actionSheet)
//        share.addAction(UIAlertAction(title: "Facebook", style: .default, handler: { (action) in
//            let post = SLComposeViewController(forServiceType: SLServiceTypeFacebook)!
//            post.setInitialText("\((self.titleLbl.text)!): \(self.contentLbl.text!)")
//            self.present(post, animated: true, completion: nil)
//        }))
//        share.addAction(UIAlertAction(title: "Twitter", style: .default, handler: { (action) in
//            let tweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
//            tweet?.setInitialText("\((self.titleLbl.text)!): \(self.contentLbl.text!)")
//            self.present(tweet!, animated: true, completion: nil)
//        }))
//        share.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
//        self.present(share, animated: true, completion: nil)
        
        // text to share
        let text = "\(self.titleLbl.text!): \(self.contentLbl.text!)"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
       activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.postToWeibo, UIActivityType.print, UIActivityType.copyToPasteboard, UIActivityType.postToFlickr, UIActivityType.postToTencentWeibo]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)

        
    }
    
    @IBOutlet weak var contentLbl: UITextView!
    
    @IBAction func deleteBtn(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            NotesData.deleteNotes(notes: self.note!)
            self.performSegue(withIdentifier: "back", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func editBtn(_ sender: Any) {
        performSegue(withIdentifier: "edit", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? NewNotesViewController{
            dest.titlee = self.titlee
            dest.content = self.content
            dest.note = self.note
        }
    }
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var navItem: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navItem.title = "\(titlee!)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        titleLbl.text = titlee!
        self.title = "\(titlee!)"
        contentLbl.text = "\(content!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
