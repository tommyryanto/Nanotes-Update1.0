//
//  ViewController.swift
//  Nanotes
//
//  Created by Tommy Ryanto on 1/3/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit
import LocalAuthentication
import NightNight

class ViewController: UIViewController {
    //========================INITIALIZING DATA=============================//
    var fetchedNote = [Note]()
    var isSearching: Bool = false
    var data: [Notes]?
    var auth: String?
    
    //================================UNWINDING SEGUE=======================//
    @IBAction func back(sender: UIStoryboardSegue){
        print("back")
    }
    
    //===================================OUTLET AND ACTION====================//
    @IBOutlet weak var searchText: UISearchBar!
    @IBOutlet weak var segmentValue: UISegmentedControl!
    @IBOutlet weak var listNotes: UITableView!
    @IBAction func segmentAction(_ sender: Any) {
        reloadData()
    }
    
    
    //=============================HIDING KEYBOARD======================//
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    //==========================DATA PROCESSING========================//
    func reloadData(){
        data = NotesData.retrieve()
        fetchedNote.removeAll()
        
        for i in data! {
            let secure = segmentValue.titleForSegment(at: segmentValue.selectedSegmentIndex) == "Secure" ? true : false
            let titleappend = i.value(forKey: "title") as! String
            let privappend = i.value(forKey: "priv") as! Bool
            let contentAppend = i.value(forKey: "notes") as! String
            if privappend && secure{
                self.fetchedNote.append(Note(title: titleappend, priv: privappend, content: contentAppend, data: i))
            }
            if !privappend && !secure{
                self.fetchedNote.append(Note(title: titleappend, priv: privappend, content: contentAppend, data: i))
            }
        }
        
        listNotes.reloadData()
        print("reloaded")
    }
    
    //===========================LIFE CYCLE==========================//
    override func viewWillAppear(_ animated: Bool) {
        reloadData()
        if UserDefaults.standard.bool(forKey: "darkmode"){
            NightNight.theme = .night
        }
        else{
            NightNight.theme = .normal
        }
        listNotes.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
        let navigationBar = navigationController?.navigationBar
        navigationBar?.mixedBarStyle = MixedBarStyle(normal: .default, night: .black)
        segmentValue.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print(Date())
        listNotes.delegate = self
        listNotes.dataSource = self
        searchText.delegate = self
        
        listNotes.mixedSeparatorColor = MixedColor(normal: 0xffffff, night: 0x000000)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//==========================ENTENSION PART=============================//

//=====================TABLE VIEW EXTENSION===========================//
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.mixedBackgroundColor = MixedColor(normal: 6750207, night: 38655)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedNote.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            NotesData.deleteNotes(notes: data![indexPath.row])
            reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listNotes.dequeueReusableCell(withIdentifier: "cell") as! CustomTableViewCell
        
        var titleCell: String
        
        titleCell = fetchedNote[indexPath.row].title
        
        cell.titleLbl.text = "\(titleCell)"
        
        if fetchedNote[indexPath.row].priv{
            cell.secure.image = UIImage(named: "lock")
        }
        else{
            cell.secure.image = nil
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if fetchedNote[indexPath.row].priv{
            let authContext = LAContext()
            var authError : NSError?
            if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError){
                /*var systemInfo = utsname()
                uname(&systemInfo)
                let machineMirror = Mirror(reflecting: systemInfo.machine)
                let identifier = machineMirror.children.reduce("") { identifier, element in
                    guard let value = element.value as? Int8, value != 0 else { return identifier }
                    return identifier + String(UnicodeScalar(UInt8(value)))
                }
                if identifier == "iPhone10,3" || identifier == "iPhone10,6"{
                    self.auth = "Face ID"
                }
                else{
                    self.auth = "Touch ID"
                }*/
                
                if UIDevice().userInterfaceIdiom == .phone{
                    if UIScreen.main.nativeBounds.height > 2435{
                        self.auth = "Face ID"
                    }
                    else{
                        self.auth = "Touch ID"
                    }
                }
                else{
                    self.auth = "Touch ID"
                }
                
                authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Use \(self.auth!)", reply: { (success, error) in
                    if success {
                        print("\(self.auth!) success")
                        DispatchQueue.main.async() {
                            self.performSegue(withIdentifier: "DetailViewController", sender: self)
                        }
                    }
                    else{
                        print("\(self.auth!) failed")
                    }
                })
            }
            else{
                print("No Authentication")
                if UserDefaults.standard.string(forKey: "pin") != nil{
                    let alert = UIAlertController(title: "", message: "Enter password", preferredStyle: .alert)
                    alert.addTextField { (textField) in
                        textField.placeholder = "Enter note password!"
                    }
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //let password = alert.textFields![0]
                        if UserDefaults.standard.string(forKey: "pin") == alert.textFields![0].text{
                            self.performSegue(withIdentifier: "DetailViewController", sender: self)
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    let alert = UIAlertController(title: "Error", message: "Input your PIN on more tab!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else{
            performSegue(withIdentifier: "DetailViewController", sender: self)
        }
        
    }
    
    //===============================PASSING DATA===========================//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = listNotes.indexPathForSelectedRow{
                destination.titlee = fetchedNote[indexPath.row].title
                destination.content = fetchedNote[indexPath.row].content
                destination.note = fetchedNote[indexPath.row].data
            }
        }
    }
}


//=============================SEARCH BAR EXTENSION=========================//
extension ViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            reloadData()
        }
        else{
            fetchedNote = fetchedNote.filter({ (note) -> Bool in
                return note.title.lowercased().contains(searchText.lowercased())
            })
        }
        listNotes.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}


//=================================NOTE CLASS DATA===========================//
class Note{
    var data: Notes
    var title: String
    var priv: Bool
    var content: String
    init(title: String, priv: Bool, content: String, data: Notes){
        self.title = title
        self.priv = priv
        self.content = content
        self.data = data
    }
}

