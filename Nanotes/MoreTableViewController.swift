//
//  MoreTableViewController.swift
//  Nanotes
//
//  Created by Tommy Ryanto on 10/6/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AssistantKit
import NightNight

class MoreTableViewController: UITableViewController {
    func darkModeNotSupported(){
        darkMode.isOn = false
        let alert = UIAlertController(title: "Error", message: "Dark Mode isn't available for this device model. You need a Plus iPhone or iPhone X to use this feature", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func darkModeSupported(){
        let manager = IQKeyboardManager.shared
        manager.enable = true
        manager.overrideKeyboardAppearance = true
        manager.keyboardAppearance = .dark
        UserDefaults.standard.set(darkMode.isOn, forKey: "darkmode")
        if darkMode.isOn{
            NightNight.theme = .night
            print("dark mode on")
            manager.overrideKeyboardAppearance = true
            manager.keyboardAppearance = .dark
        }
        else{
            NightNight.theme = .normal
            manager.overrideKeyboardAppearance = true
            manager.keyboardAppearance = .default
        }
        let view = UIView()
        view.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
        let navigationBar = navigationController?.navigationBar
        navigationBar?.mixedBarStyle = MixedBarStyle(normal: .default, night: .black)
        tableView.mixedSeparatorColor = MixedColor(normal: 0x000000, night: 0x000000)
        tableView.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
        let tabBar = tabBarController?.tabBar
        tabBar?.mixedBarTintColor = MixedColor(normal: 0xffffff, night: 0x000000)
    }
    
    @IBOutlet weak var darkMode: UISwitch!
    @IBAction func darkModeAction(_ sender: Any) {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        if UIDevice().userInterfaceIdiom == .phone{
            if UIScreen.main.nativeBounds.height >= 1920{
                if identifier == "iPhone7,1"{
                    //iPhone 6 Plus
                    darkModeNotSupported()
                }
                else{
                    //supported dark mode device model
                    darkModeSupported()
                }
            }
            else{
                //non-Plus iPhone
                darkModeNotSupported()
            }
        }
        else if UIDevice().userInterfaceIdiom == .pad{
            if Device.version == .padPro{
                darkModeSupported()
            }
            else{
                darkModeNotSupported()
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.mixedBackgroundColor = MixedColor(normal: 6750207, night: 38655)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 0{
                print("called")
                let pin = UIAlertController(title: "Enter PIN", message: "Your PIN will be used for unlocking private mode(instead of Touch/Face ID)", preferredStyle: .alert)
                let setPin = UIAlertAction(title: "Set PIN", style: .default, handler: { (action) in
                    if pin.textFields![0].text != pin.textFields![1].text {
                        print("PIN error")
                        pin.message = "PIN doesn't match!"
                    }
                    else{
                        print("PIN set")
                        UserDefaults.standard.set(pin.textFields![0].text, forKey: "pin")
                    }
                })
                if UserDefaults.standard.string(forKey: "pin") != nil{
                    pin.addTextField { (textfield) in
                        textfield.placeholder = "Enter your previous PIN here"
                        textfield.isSecureTextEntry = true
                        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textfield, queue: OperationQueue.main) { notification in
                            setPin.isEnabled = pin.textFields![2].text == pin.textFields![1].text && pin.textFields![0].text == UserDefaults.standard.string(forKey: "pin")
                        }
                    }
                }
                pin.addTextField { (textfield) in
                    textfield.placeholder = "Enter your private PIN here"
                    textfield.isSecureTextEntry = true
                    NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textfield, queue: OperationQueue.main) { notification in
                        if UserDefaults.standard.string(forKey: "pin") == nil{
                            setPin.isEnabled = textfield.text! == pin.textFields![1].text
                        }
                        else{
                            setPin.isEnabled = pin.textFields![2].text == pin.textFields![1].text && pin.textFields![0].text == UserDefaults.standard.string(forKey: "pin")
                        }
                    }
                }
                pin.addTextField { (textfield) in
                    textfield.placeholder = "Re-enter your PIN here"
                    textfield.isSecureTextEntry = true
                    NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textfield, queue: OperationQueue.main) { notification in
                        if UserDefaults.standard.string(forKey: "pin") == nil{
                            setPin.isEnabled = textfield.text! == pin.textFields![0].text
                        }
                        else{
                            setPin.isEnabled = textfield.text == pin.textFields![1].text && pin.textFields![0].text == UserDefaults.standard.string(forKey: "pin")
                        }
                    }
                }
                pin.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                setPin.isEnabled = false
                pin.addAction(setPin)
                self.present(pin, animated: true, completion: nil)
            }
            else if indexPath.row == 1{
                if UserDefaults.standard.string(forKey: "pin") == nil{
                    tableView.deselectRow(at: indexPath, animated: true)
                }
                else{
                    let alert = UIAlertController(title: "Reset PIN", message: "This will remove PIN for all notes", preferredStyle: .alert)
                    let reset = UIAlertAction(title: "Reset PIN", style: .destructive) { (action) in
                        UserDefaults.standard.removeObject(forKey: "pin")
                    }
                    alert.addTextField { (textfield) in
                        textfield.placeholder = "Enter your previous PIN"
                        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textfield, queue: OperationQueue.main) { notification in
                            reset.isEnabled = textfield.text == UserDefaults.standard.string(forKey: "pin")
                        }
                    }
                    reset.isEnabled = false
                    alert.addAction(reset)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "darkmode"){
            darkModeSupported()
        }
        else{
            darkMode.isOn = false
            darkModeSupported()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
