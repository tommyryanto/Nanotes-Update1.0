//
//  NotesData.swift
//  Nanotes
//
//  Created by Tommy Ryanto on 7/5/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit
import CoreData

class NotesData: NSObject {
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.persistentContainer.viewContext
    }
    
    class func updateNotes(note: Notes, notesContent: String, title: String, priv: Bool, date: Date) -> Bool{
        note.setValue(notesContent, forKey: "notes")
        note.setValue(title, forKey: "title")
        note.setValue(priv, forKey: "priv")
        note.setValue(date, forKey: "date")
        
        return true
    }
    
    class func saveNotes(notes: String, title: String, priv: Bool, date: Date) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Notes", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(notes, forKey: "notes")
        manageObject.setValue(title, forKey: "title")
        manageObject.setValue(priv, forKey: "priv")
        manageObject.setValue(date, forKey: "date")
        
        do{
            try context.save()
            return true
        }
        catch{
            return false
        }
    }
    
    class func deleteNotes(notes: Notes){
        let context = getContext()
        
        context.delete(notes)
    }
    
    class func retrieve() -> [Notes]? {
        let context = getContext()
        var notes: [Notes]?
        
        do{
           notes = try context.fetch(Notes.fetchRequest())
            return notes
        }catch{
            return notes
        }
    }
}
