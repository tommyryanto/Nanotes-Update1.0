//
//  NewNotesViewController.swift
//  Nanotes
//
//  Created by Tommy Ryanto on 13/3/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit
import NightNight

class NewNotesViewController: UIViewController {

    var note: Notes? = nil
    var titlee: String?
    var content: String?
    
    @IBAction func addnote(_ sender: Any) {
        var validation: Bool = true
        var alert: UIAlertController?
        if titleText.text == "" {
            alert = UIAlertController(title: "Error", message: "Input the title!", preferredStyle: .alert)
            validation = false
        }
        else if contentField.text == ""{
            alert = UIAlertController(title: "Error", message: "Input the content!", preferredStyle: .alert)
            validation = false
        }
        if validation == false{
            alert!.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert!, animated: true, completion: nil)
        }
        else if validation == true{
            let priv = privateSwitch.isOn ? true : false
            let title = titleText.text
            let content = contentField.text
            let time = Date()
            
            if note == nil{
                if NotesData.saveNotes(notes: content!, title: title!, priv: priv, date: time){
                    print("Data accepted")
                    var titleArray: [String] = []
                    
                    if UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.value(forKey: "titleNote") != nil{
                        titleArray = UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.value(forKey: "titleNote") as! [String]
                    }
                    
                    //UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.setValue(title, forKey: "title")
                    titleArray.append(title!)
                    UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.setValue(titleArray, forKey: "titleNote")
                    
                    for i in UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.value(forKey: "titleNote") as! [String]{
                        print(i)
                    }
                    
                    self.performSegue(withIdentifier: "back", sender: self)
                    print("perform segue called")
                }
                else{
                    let alert1 = UIAlertController(title: "Error", message: "The Data can't be read", preferredStyle: .alert)
                    alert1.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    present(alert1, animated: true, completion: nil)
                }
            }
            else if note != nil{
                if NotesData.updateNotes(note: self.note!, notesContent: content!, title: title!, priv: priv, date: time){
                    print("Data accepted")
                    performSegue(withIdentifier: "backToDetail", sender: self)
                }
            }
        }
        /*tambah view
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            label.center = CGPoint(x: 160, y: 285)
            label.textAlignment = .center
            label.text = "I'm a test label"
            self.view.addSubview(label)
         */
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? DetailViewController {
            dest.titlee = titleText.text
            dest.content = contentField.text
        }
    }
    
    @IBOutlet weak var contentField: UITextView!
    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var titleText: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        if note != nil {
            contentField.text = content
            titleText.text = titlee
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.bool(forKey: "darkmode"){
            print("dark mode theme")
            configDarkmode()
        }
        else{
            print("dark mode off")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configDarkmode(){
        let view = UIView()
        view.mixedBackgroundColor = MixedColor(normal: 0xffffff, night: 0x000000)
        let navigationBar = navigationController?.navigationBar
        navigationBar?.mixedBarStyle = MixedBarStyle(normal: .default, night: .black)
        contentField.mixedBackgroundColor = MixedColor(normal: 0, night: 42927)
    }

}
