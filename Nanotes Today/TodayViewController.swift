//
//  TodayViewController.swift
//  Nanotes Today
//
//  Created by Tommy Ryanto on 20/6/18.
//  Copyright © 2018 Tommy Ryanto. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var titleList: UITableView!
    
    var note: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        note?.removeAll()
        note = (UserDefaults.init(suiteName: "group.com.TommyR.Nanotes")?.value(forKey: "titleNote") as! [String])
        
        for i in note!{
            print(i)
        }
        
        titleList.dataSource = self
        titleList.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (note?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = titleList.dequeueReusableCell(withIdentifier: "todayCell")
        cell?.textLabel?.text = note?[indexPath.row]
        
        return cell!
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
